let playing = false;
let X = document.getElementById('X');
let O = document.getElementById('O');

const cellI = document.getElementsByClassName('cell')
const X_class = cellI.innerHTML = "X";
const O_class = cellI.textContent = "O";
let O_turn;
const cellElement = document.querySelectorAll('div.cell')

cellElement.forEach(cell => {
    cell.addEventListener('click', handleclicked)
})
let startReset = document.getElementById("startreset");
let check = document.getElementById("check");

//console.log(X_class)		    

startReset.addEventListener('click', startResetGame);
//check.addEventListener('click', fn);
//helper fuctions
function setText(id, text) {
    document.getElementById(id).innerHTML = text;
}

function show(id) {
    document.getElementById(id).style.display = 'block';
}


function hide(id) {
    document.getElementById(id).style.display = 'none';
}

function handleclicked(e) {
    console.log('CLICKED')
    const cell = e.target;

    //    const currentclass = X_class;
    if (cell.innerHTML == "") {
        cell.innerHTML = O_turn ? O_class : X_class;
        //    console.log(cell)
        //    console.log(currentclass);
        //    placemark(cell, currentclass);
        //        show('X');//        hide('O');
        swapturns();
        winnner();
        if (cell.innerHTML == X_class) {
            show('O');
            hide('X');
        } else {
            show('X');
            hide('O');
        }
    }
    // let currentclass = cell.innerHTML;
    //console.log(currentclass);
    //else {
    //        show('O');
    //        hide('X');
    //    }

}

//function placemark(cell, currentclass) {
//    cell.innerHTML = "X";
//}

function swapturns() {
    O_turn = !O_turn;
}

function startResetGame(e) {
    if (playing === true) {
        //game is on and you want to reset
        setText("startreset", "Start Game");
        // currentclass = X_class;
        clearAllCell();
        hide('gameover');
        show('X')
        show('O')
        CellNormal()

    } else {
        //game is off and you want to start a new one 
        setText("startreset", "Reset Game");
        clearAllCell();
        hide("gameover");
        CellNormal();
    }

    playing = !playing;

}

function getBoxNo(no) {
    let y;
    y = document.getElementById('cell' + no)
    return y.innerHTML
}


function checkMove(box1, box2, box3, sign) {
    if (getBoxNo(box1) == sign && getBoxNo(box2) == sign && getBoxNo(box3) == sign) {
        document.getElementById('cell' + box1).style.background = "rgba(0, 0, 0, 1)";
        document.getElementById('cell' + box2).style.background = "rgba(0, 0, 0, 1)";
        document.getElementById('cell' + box3).style.background = "rgba(0, 0, 0, 1)";
        document.getElementById('cell' + box1).style.color = "white";
        document.getElementById('cell' + box2).style.color = "white";
        document.getElementById('cell' + box3).style.color = "white";

        return true;
    } else {
        return false;
    }
}


function winnner() {
    if (checkMove(1, 2, 3, "X") || checkMove(4, 5, 6, "X") || checkMove(7, 8, 9, "X") || checkMove(1, 4, 7, "X") || checkMove(2, 5, 8, "X") || checkMove(3, 6, 9, "X") || checkMove(1, 5, 9, "X") || checkMove(3, 5, 7, "X")) {
        show('gameover')
        setText("gameover", "<p>X Winner</p>");
        playing = false;

    } else if ((checkMove(1, 2, 3, "O") || checkMove(4, 5, 6, "O") || checkMove(7, 8, 9, "O") || checkMove(1, 4, 7, "O") || checkMove(2, 5, 8, "O") || checkMove(3, 6, 9, "O") || checkMove(1, 5, 9, "O") || checkMove(3, 5, 7, "O"))) {
        show('gameover')
        setText("gameover", "<p>O Winner</p>");
        playing = false;
    } else {

    }

}

function clearAllCell() {
    for (let i = 1; i <= 9; i++) {

        document.getElementById('cell' + i).innerHTML = "";

        document.getElementById('cell' + i).style.background = "#fff";
    }
}

function CellNormal() {
    for (let i = 1; i <= 9; i++) {
        document.getElementById('cell' + i).style.color = "#000";
    }
}
